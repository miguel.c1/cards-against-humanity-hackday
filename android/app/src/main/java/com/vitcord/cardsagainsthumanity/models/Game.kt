package com.vitcord.cardsagainsthumanity.models

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

data class Game(
    val players: List<Player>,
    val currentTurn: Turn,
    val state: GameState,
    val blackDeck: Int,
    val whiteDeck: Int
)

data class Player(
    val name: String,
    val awesomePoints: Int,
    val hand: List<WhiteCard>
)

data class Turn(
    val round: Int,
    val cardCzar: String,
    val blackCard: BlackCard?,
    val submissions: List<PlayerWhiteCardSubmission>,
    val state: TurnState
)

data class WhiteCard(
    val value: String,
    var selected: Boolean
)

sealed class BlackCard(
    val statement: String
) {
    companion object {
        const val PICK_ONE = "pick_one"
        const val PICK_TWO = "pick_two"
    }

    class PickOne(
        statement: String
    ) : BlackCard(statement)

    class PickTwo(
        statement: String
    ) : BlackCard(statement)
}

sealed class PlayerWhiteCardSubmission(
    val player: String,
    val selected: Boolean
) {
    class PlayerPickOneSubmission(
        val submission: WhiteCard,
        player: String,
        selected: Boolean = false
    ) : PlayerWhiteCardSubmission(player, selected)

    class PlayerPickTwoSubmission(
        val submission: Pair<WhiteCard, WhiteCard>,
        player: String,
        selected: Boolean = false
    ) : PlayerWhiteCardSubmission(player, selected)
}

sealed class TurnState {
    companion object {
        const val WAITING = "waiting_for_white_cards"
        const val CHOOSING = "choosing_best_response"
    }

    object WaitingForWhiteCards : TurnState()
    object ChoosingBestResponse : TurnState()
}

sealed class GameState {
    companion object {
        const val WAITING = "waiting_for_players"
        const val PLAYING = "playing_turn"
        const val FINISHED = "game_finished"
    }

    object WaitingForPlayers : GameState()
    object PlayingTurn : GameState()
    object GameFinished : GameState()
}

fun parseGame(json: JSONObject): Game {
    val blackDeck = json.getInt("blackDeck")
    val whiteDeck = json.getInt("whiteDeck")

    val players = json.getJSONArray("players").map(::parsePlayer)
    val currentTurn =
        parseTurn(json.getJSONObject("currentTurn"))
    val state =
        parseGameState(json.getString("state"))

    return Game(
        players,
        currentTurn,
        state,
        blackDeck,
        whiteDeck
    )
}

fun parsePlayer(json: JSONObject): Player {
    val awesomePoints = json.getInt("awesomePoints")
    val name = json.getString("name")
    val hand = json.getJSONArray("hand").map(::parseWhiteCard)

    return Player(
        name,
        awesomePoints,
        hand
    )
}

fun parseGameState(string: String): GameState = when (string) {
    GameState.WAITING -> GameState.WaitingForPlayers
    GameState.PLAYING -> GameState.PlayingTurn
    else -> GameState.GameFinished
}

fun parseTurnState(string: String): TurnState = when (string) {
    TurnState.WAITING -> TurnState.WaitingForWhiteCards
    else -> TurnState.ChoosingBestResponse
}

fun parseTurn(json: JSONObject): Turn {
    val round = json.getInt("round")
    val cardCzar = json.getString("cardCzar")
    val state =
        parseTurnState(json.getString("state"))
    val blackCard = parseBlackCard(
        json.getJSONObjectOrNull("blackCard")
    )
    val submissions = if (blackCard == null) emptyList() else json.getJSONArray("submissions")
        .map<JSONArray, PlayerWhiteCardSubmission> { obj ->
            parsePlayerSubmission(
                obj,
                blackCard
            )
        }

    return Turn(
        round,
        cardCzar,
        blackCard,
        submissions,
        state
    )
}

fun parseBlackCard(json: JSONObject?): BlackCard? {
    if (json == null) return null

    val statement = json.getString("statement")
    val type = json.getString("type")

    return if (type == BlackCard.PICK_TWO)
        BlackCard.PickTwo(statement)
    else
        BlackCard.PickOne(statement)
}

fun parseWhiteCard(json: JSONObject): WhiteCard {
    val value = json.getString("value")
    return WhiteCard(value, false)
}

fun parsePlayerSubmission(jsonArray: JSONArray, blackCard: BlackCard): PlayerWhiteCardSubmission =
    when (blackCard) {
        is BlackCard.PickOne -> PlayerWhiteCardSubmission.PlayerPickOneSubmission(
            parseWhiteCard(jsonArray.getJSONObject(0)),
            parsePlayer(jsonArray.getJSONObject(1)).name
        )
        is BlackCard.PickTwo -> {
            val whiteCards = jsonArray.getJSONArray(0)
            val firstWhiteCard = parseWhiteCard(whiteCards.getJSONObject(0))
            val secondWhiteCard = parseWhiteCard(whiteCards.getJSONObject(1))
            val player = parsePlayer(jsonArray.getJSONObject(1))

            PlayerWhiteCardSubmission.PlayerPickTwoSubmission(Pair(firstWhiteCard, secondWhiteCard), player.name)
        }
    }


fun JSONObject.getJSONObjectOrNull(key: String): JSONObject? = try {
    getJSONObject(key)
} catch (ex: JSONException) {
    null
}

fun <A, B> JSONArray.map(f: (A) -> B): List<B> = if (length() == 0) emptyList() else
    (0 until length()).map { index -> f(get(index) as A) }