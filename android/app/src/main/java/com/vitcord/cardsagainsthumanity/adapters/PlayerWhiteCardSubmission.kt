
package com.vitcord.cardsagainsthumanity.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vitcord.cardsagainsthumanity.R
import com.vitcord.cardsagainsthumanity.models.PlayerWhiteCardSubmission
import com.vitcord.cardsagainsthumanity.models.WhiteCard
import kotlinx.android.synthetic.main.card_item.view.*

class PlayerWhiteCardSubmissionAdapter(private val onClick: (PlayerWhiteCardSubmission) -> Unit) : ListAdapter<PlayerWhiteCardSubmission, PlayerWhiteCardSubmissionViewHolder>(diff) {
    companion object {
        val diff = object:DiffUtil.ItemCallback<PlayerWhiteCardSubmission>() {
            override fun areItemsTheSame(oldItem: PlayerWhiteCardSubmission, newItem: PlayerWhiteCardSubmission): Boolean {
                return oldItem.player == newItem.player && when(oldItem) {
                    is PlayerWhiteCardSubmission.PlayerPickOneSubmission -> oldItem.submission.value == (newItem as PlayerWhiteCardSubmission.PlayerPickOneSubmission).submission.value
                    is PlayerWhiteCardSubmission.PlayerPickTwoSubmission -> oldItem.submission.first.value == (newItem as PlayerWhiteCardSubmission.PlayerPickTwoSubmission).submission.first.value && oldItem.submission.second.value == (newItem as PlayerWhiteCardSubmission.PlayerPickTwoSubmission).submission.second.value
                }
            }

            override fun areContentsTheSame(oldItem: PlayerWhiteCardSubmission, newItem: PlayerWhiteCardSubmission): Boolean {
                return when(oldItem) {
                    is PlayerWhiteCardSubmission.PlayerPickOneSubmission -> when(newItem) {
                        is PlayerWhiteCardSubmission.PlayerPickOneSubmission -> oldItem.submission == newItem.submission && oldItem.player == newItem.player && oldItem.selected == newItem.selected
                        is PlayerWhiteCardSubmission.PlayerPickTwoSubmission -> false
                    }
                    is PlayerWhiteCardSubmission.PlayerPickTwoSubmission ->  when(newItem) {
                        is PlayerWhiteCardSubmission.PlayerPickOneSubmission -> false
                        is PlayerWhiteCardSubmission.PlayerPickTwoSubmission -> oldItem.submission == newItem.submission && oldItem.player == newItem.player && oldItem.selected == newItem.selected
                    }
                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerWhiteCardSubmissionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false)
        return PlayerWhiteCardSubmissionViewHolder(
            view,
            onClick
        )
    }

    override fun onBindViewHolder(holder: PlayerWhiteCardSubmissionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class PlayerWhiteCardSubmissionViewHolder(itemView: View, private val onClick: (PlayerWhiteCardSubmission) -> Unit) : RecyclerView.ViewHolder(itemView) {
    fun bind(playerWhiteCardSubmission: PlayerWhiteCardSubmission) {
        with(itemView) {
            setOnClickListener{ onClick(playerWhiteCardSubmission) }

            if(playerWhiteCardSubmission is PlayerWhiteCardSubmission.PlayerPickOneSubmission)
                cardText.text = playerWhiteCardSubmission.submission.value
            if (playerWhiteCardSubmission.selected)
                cardViewConstraintLayout.background = context.getColor(R.color.colorPrimary).toDrawable()
            else
                cardViewConstraintLayout.background = Color.TRANSPARENT.toDrawable()
        }
    }
}