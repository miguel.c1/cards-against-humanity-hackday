package com.vitcord.cardsagainsthumanity

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.vitcord.cardsagainsthumanity.fragments.JoiningFragment
import com.vitcord.cardsagainsthumanity.models.Game
import com.vitcord.cardsagainsthumanity.models.parseGame
import org.json.JSONObject
import java.net.URISyntaxException


class MainActivity : AppCompatActivity() {

    lateinit var mSocket: Socket
    var gameStatusListener: GameStatusListener? = null

    private val newGameStatusSocketListener = Emitter.Listener { args ->
        val json: JSONObject = args[0] as JSONObject
        val game = parseGame(json)

        gameStatusListener?.onNewGameStatus(game)
    }

    private val gameErrorListener = Emitter.Listener { args ->
        val json: JSONObject = args[0] as JSONObject
        val msg: String = json.getString("msg")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        val fragment = JoiningFragment.newInstance()
        supportFragmentManager
            .beginTransaction().replace(R.id.fragmentContainer, fragment)
            .commit()

        try {
            mSocket = IO.socket("https://e7c800c2.ngrok.io")
            mSocket.connect()
            mSocket.on("new_game_status", newGameStatusSocketListener)
            mSocket.on("game_error", gameErrorListener)
        } catch (e: URISyntaxException) { }
    }

    interface GameStatusListener {
        fun onNewGameStatus(game: Game)
    }
}
