package com.vitcord.cardsagainsthumanity.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.beust.klaxon.Klaxon
import com.vitcord.cardsagainsthumanity.MainActivity
import com.vitcord.cardsagainsthumanity.R
import com.vitcord.cardsagainsthumanity.adapters.PlayerWhiteCardSubmissionAdapter
import com.vitcord.cardsagainsthumanity.models.Game
import com.vitcord.cardsagainsthumanity.models.GameState
import com.vitcord.cardsagainsthumanity.models.PlayerWhiteCardSubmission
import com.vitcord.cardsagainsthumanity.models.TurnState
import com.vitcord.cardsagainsthumanity.requests.PlayerWhiteCardSubmissionRequest
import kotlinx.android.synthetic.main.fragment_czar.*

class CzarFragment : Fragment(), MainActivity.GameStatusListener {

    private var game: Game? = null
    private var isSubmissionSelected = false
    private var selectedSubmission: PlayerWhiteCardSubmission? = null

    private var playerWhiteCardSubmissionAdapter: PlayerWhiteCardSubmissionAdapter =
        PlayerWhiteCardSubmissionAdapter { playerWhiteCardSubmission ->
            onClickItem(playerWhiteCardSubmission)
        }

    companion object {
        fun newInstance(name: String) : CzarFragment {
            val fragment =
                CzarFragment()
            val arguments = Bundle()
            arguments.putString(playerName, name)
            fragment.arguments = arguments
            return fragment
        }
        private const val playerName: String = "playerName"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity as MainActivity).gameStatusListener = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_czar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardsRecyclerView.adapter = playerWhiteCardSubmissionAdapter
        cardsRecyclerView.layoutManager = GridLayoutManager(context, 2)

        waitingForPlayersWhiteCardsText.visibility = View.VISIBLE

        selectCardButton.setOnClickListener {
            selectCardButton.visibility = View.GONE
            playerWhiteCardSubmissionAdapter.submitList(emptyList())

            (activity as MainActivity).mSocket.emit("czar_winner_choice",
                Klaxon().toJsonString(
                    PlayerWhiteCardSubmissionRequest(
                        selectedSubmission!!.player,
                        (selectedSubmission as PlayerWhiteCardSubmission.PlayerPickOneSubmission).submission,
                        arguments!!.getString(playerName)
                    )
                ))
        }
    }

    override fun onNewGameStatus(game: Game) {
        activity!!.runOnUiThread {
            this.game = game
            if (game.state == GameState.PlayingTurn) {
                if (this.game!!.currentTurn.state == TurnState.ChoosingBestResponse) {
                    waitingForPlayersWhiteCardsText.visibility = View.GONE
                    playerWhiteCardSubmissionAdapter.submitList(this.game!!.currentTurn.submissions)
                }
                if (this.game!!.currentTurn.cardCzar != arguments!!.getString(playerName)) {
                    fragmentManager!!.popBackStack()
                }
            }
        }
    }

    fun onClickItem(playerWhiteCardSubmission: PlayerWhiteCardSubmission) {
        selectCardButton.visibility = View.VISIBLE

        val updatedSelectedSubmission = when(playerWhiteCardSubmission) {
            is PlayerWhiteCardSubmission.PlayerPickOneSubmission -> PlayerWhiteCardSubmission.PlayerPickOneSubmission(
                playerWhiteCardSubmission.submission, playerWhiteCardSubmission.player, true
            )
            is PlayerWhiteCardSubmission.PlayerPickTwoSubmission -> PlayerWhiteCardSubmission.PlayerPickTwoSubmission(
                playerWhiteCardSubmission.submission, playerWhiteCardSubmission.player, true
            )
        }
        isSubmissionSelected = true
        selectedSubmission = updatedSelectedSubmission

        val updatedTurn = this.game!!.currentTurn.copy(submissions = this.game!!.currentTurn.submissions.map {
            playerSubmission -> if(playerSubmission.player == playerWhiteCardSubmission.player) updatedSelectedSubmission else playerSubmission
        })

        this.game = this.game!!.copy(currentTurn = updatedTurn)

        playerWhiteCardSubmissionAdapter.submitList(this.game!!.currentTurn.submissions)
    }
}
