package com.vitcord.cardsagainsthumanity.requests

data class JoinGameRequest(
    val name: String
)