package com.vitcord.cardsagainsthumanity.requests

import com.vitcord.cardsagainsthumanity.models.Player
import com.vitcord.cardsagainsthumanity.models.WhiteCard

data class WhiteCardSubmissionRequest(
    val player: Player,
    val whiteCard: WhiteCard
)