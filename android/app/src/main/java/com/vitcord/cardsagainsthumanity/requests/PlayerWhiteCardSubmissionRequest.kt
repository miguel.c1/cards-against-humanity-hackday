package com.vitcord.cardsagainsthumanity.requests

import com.vitcord.cardsagainsthumanity.models.WhiteCard

data class PlayerWhiteCardSubmissionRequest(
    val playerName: String,
    val whiteCard: WhiteCard,
    val czarName: String
)