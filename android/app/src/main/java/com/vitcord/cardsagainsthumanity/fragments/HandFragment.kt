package com.vitcord.cardsagainsthumanity.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.beust.klaxon.Klaxon
import com.vitcord.cardsagainsthumanity.MainActivity
import com.vitcord.cardsagainsthumanity.R
import com.vitcord.cardsagainsthumanity.adapters.WhiteCardAdapter
import com.vitcord.cardsagainsthumanity.models.Game
import com.vitcord.cardsagainsthumanity.models.GameState
import com.vitcord.cardsagainsthumanity.models.TurnState
import com.vitcord.cardsagainsthumanity.models.WhiteCard
import com.vitcord.cardsagainsthumanity.requests.WhiteCardSubmissionRequest
import kotlinx.android.synthetic.main.fragment_hand.*

class HandFragment : Fragment(),
    MainActivity.GameStatusListener {

    private var game: Game? = null
    private var isWhiteCardSelected = false
    private var selectedWhiteCard: WhiteCard? = null
    private var permittedNextRound = true

    private var whiteCardAdapter: WhiteCardAdapter =
        WhiteCardAdapter { whiteCard ->
            onClickItem(whiteCard)
        }

    companion object {
        fun newInstance(name: String) : HandFragment {
            val fragment =
                HandFragment()
            val arguments = Bundle()
            arguments.putString(playerName, name)
            fragment.arguments = arguments
            return fragment
        }
        private const val playerName: String = "playerName"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_hand, container, false)
    }

    override fun onResume() {
        super.onResume()

        (activity as MainActivity).gameStatusListener = this
        (activity as MainActivity).mSocket.emit("refresh_game")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardsRecyclerView.adapter = whiteCardAdapter
        cardsRecyclerView.layoutManager = GridLayoutManager(context, 2)


        selectCardButton.setOnClickListener {
            selectCardButton.visibility = View.GONE
            whiteCardAdapter.submitList(emptyList())
            cardSubmittedText.visibility = View.VISIBLE

            val currentPlayer = game!!.players.find { player -> player.name == arguments!!.getString(
                playerName
            ) }
            (activity as MainActivity).mSocket.emit("white_card_submission",
                Klaxon().toJsonString(
                    WhiteCardSubmissionRequest(
                        currentPlayer!!,
                        selectedWhiteCard!!
                    )
                ))
        }
    }

    override fun onNewGameStatus(game: Game) {
        this.game = game

        if (this.game!!.currentTurn.cardCzar == arguments!!.getString(playerName)) {
            val fragment = CzarFragment.newInstance(arguments!!.getString(playerName))
            fragmentManager!!.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(null)
                .commit()
        } else {
            activity!!.runOnUiThread {
                if (game.state == GameState.PlayingTurn && permittedNextRound) {
                    waitingForPlayersText.visibility = View.GONE
                    cardSubmittedText.visibility = View.GONE
                    permittedNextRound = false
                    val player = game.players.find { player -> player.name == arguments!!.getString(
                        playerName) }
                    whiteCardAdapter.submitList(player!!.hand)
                }
                if(game.currentTurn.state == TurnState.ChoosingBestResponse)
                    permittedNextRound = true
            }
        }
    }

    fun onClickItem(whiteCard: WhiteCard) {
        val updatedWhiteCard = whiteCard.copy(selected=true)
        isWhiteCardSelected = true
        selectedWhiteCard = updatedWhiteCard
        selectCardButton.visibility = View.VISIBLE

        val currentPlayer = game!!.players.find { player -> player.name == arguments!!.getString(
            playerName
        ) }
        val updatedCurrentPlayer = currentPlayer!!.copy(hand=currentPlayer!!.hand.map {
                card -> if (card.value == updatedWhiteCard.value) updatedWhiteCard else card.copy(selected=false)
        })
        game = game!!.copy(players = game!!.players.map {
                player -> if(player.name == updatedCurrentPlayer.name) updatedCurrentPlayer else player
        })

        whiteCardAdapter.submitList(updatedCurrentPlayer.hand)
    }
}
