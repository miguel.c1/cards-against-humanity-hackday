package com.vitcord.cardsagainsthumanity.fragments


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.beust.klaxon.Klaxon
import com.vitcord.cardsagainsthumanity.models.Game
import com.vitcord.cardsagainsthumanity.models.GameState
import com.vitcord.cardsagainsthumanity.MainActivity
import com.vitcord.cardsagainsthumanity.R
import com.vitcord.cardsagainsthumanity.requests.JoinGameRequest
import kotlinx.android.synthetic.main.fragment_joining.*

class JoiningFragment : Fragment(),
    MainActivity.GameStatusListener {

    private var gameJoined: Boolean = false

    companion object {
        fun newInstance(): JoiningFragment {
            return JoiningFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_joining, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        (activity as MainActivity).gameStatusListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        joiningButton.isEnabled = false
        joiningButton.setOnClickListener {
            gameJoined = true
            val name = playerName.text.toString()
            (activity as MainActivity).mSocket.emit("join_game",
                Klaxon().toJsonString(
                    JoinGameRequest(
                        name
                    )
                ))
        }
        playerName.addTextChangedListener(object:TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                joiningButton.isEnabled = !s.isNullOrEmpty()
            }

        })
    }

    override fun onNewGameStatus(game: Game) {
        if (game.state == GameState.WaitingForPlayers && gameJoined) {
            val fragment = HandFragment.newInstance(playerName.text.toString())
            fragmentManager!!.beginTransaction().replace(R.id.fragmentContainer, fragment)
                .commit()
        }
    }
}
