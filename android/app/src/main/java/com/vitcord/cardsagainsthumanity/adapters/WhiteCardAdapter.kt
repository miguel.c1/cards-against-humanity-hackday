package com.vitcord.cardsagainsthumanity.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vitcord.cardsagainsthumanity.R
import com.vitcord.cardsagainsthumanity.models.WhiteCard
import kotlinx.android.synthetic.main.card_item.view.*

class WhiteCardAdapter(private val onClick: (WhiteCard) -> Unit) : ListAdapter<WhiteCard, WhiteCardViewHolder>(diff) {
    companion object {
        val diff = object:DiffUtil.ItemCallback<WhiteCard>() {
            override fun areItemsTheSame(oldItem: WhiteCard, newItem: WhiteCard): Boolean {
                return oldItem.value == newItem.value
            }

            override fun areContentsTheSame(oldItem: WhiteCard, newItem: WhiteCard): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WhiteCardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false)
        return WhiteCardViewHolder(
            view,
            onClick
        )
    }

    override fun onBindViewHolder(holder: WhiteCardViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class WhiteCardViewHolder(itemView: View, private val onClick: (WhiteCard) -> Unit) : RecyclerView.ViewHolder(itemView) {
    fun bind(whiteCard: WhiteCard) {
        with(itemView) {
            setOnClickListener{ onClick(whiteCard) }

            cardText.text = whiteCard.value
            if (whiteCard.selected)
                cardViewConstraintLayout.background = context.getColor(R.color.colorPrimary).toDrawable()
            else
                cardViewConstraintLayout.background = Color.TRANSPARENT.toDrawable()
        }
    }
}