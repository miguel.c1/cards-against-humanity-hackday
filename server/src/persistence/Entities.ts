import mongoose, { mongo } from "mongoose";
import { WhiteCard, BlackCard, PickOne, PickTwo } from "../game/Game";

type WhiteCardEntityType = mongoose.Document & {
    value: string;
}

type BlackCardType = "pick_one" | "pick_two"
type BlackCardEntityType = mongoose.Document & {
    statement: string;
    type: BlackCardType;
}

export function mapWhiteCardEntityToDomain(entity: WhiteCardEntityType): WhiteCard {
    return { value: entity.value } as WhiteCard;
}

export function mapBlackCardEntityToDomain(entity: BlackCardEntityType): BlackCard {
    switch (entity.type) {
        case "pick_one": return { statement: entity.statement, type: entity.type } as PickOne;
        case "pick_two": return { statement: entity.statement, type: entity.type } as PickTwo;
    }
}

const whiteCardSchema = new mongoose.Schema({
    value: String
});

const blackCardSchema = new mongoose.Schema({
    statement: String,
    type: String
});

export const WhiteCardEntity = mongoose.model<WhiteCardEntityType>("WhiteCard", whiteCardSchema);
export const BlackCardEntity = mongoose.model<BlackCardEntityType>("BlackCard", blackCardSchema);