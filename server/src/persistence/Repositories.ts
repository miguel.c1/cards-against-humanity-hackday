import { WhiteCard, BlackCard, BlackDeck, WhiteDeck } from "../game/Game";
import { WhiteCardEntity, mapWhiteCardEntityToDomain, BlackCardEntity, mapBlackCardEntityToDomain } from "./Entities";

export class CardsRepository {

    public findAllWhitecards(): Promise<WhiteDeck> {
        return new Promise((resolve, reject) => {
            WhiteCardEntity.find((err, res) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(res.map((entity) => mapWhiteCardEntityToDomain(entity)));
            });
        });
    }

    public findAllBlackcards(): Promise<BlackDeck> {
        return new Promise((resolve, reject) => {
            BlackCardEntity.find((err, res) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(res.map((entity) => mapBlackCardEntityToDomain(entity)));
            });
        });
    }

    public findOnePickBlackcards(): Promise<BlackDeck> {
        return new Promise((resolve, reject) => {
            BlackCardEntity.find({ type: "pick_one" }, (err, res) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(res.map((entity) => mapBlackCardEntityToDomain(entity)));
            });
        });
    }

}