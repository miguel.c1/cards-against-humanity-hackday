import { GameSnapshot, PlayerJoined, GameStarted, NextTurn, CzarChosen, WhiteCardDealed, BlackCardDrawed, PlayerSubmittedWhiteCard, CzarChoseRoundWinner, GameCreated, WaitingForPlayers, WaitingForWhiteCards, ChoosingBestResponse } from "./game/States";
import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import { Game, createEmptyGame, PickOne, chooseCzarForRound, showJsonGame, BlackDeck, WhiteDeck, PlayerWhiteCardSubmission, Player } from "./game/Game";
import http from "http";
import ioServer from "socket.io";
import mongoose from "mongoose";
import { CardsRepository } from "./persistence/Repositories";
import { shuffle } from "lodash";
import path from "path";

const gameSnapshot = new GameSnapshot();
let activeGame: Game | null = null;

const app = express();

app.set("view engine", "ejs");
app.set("views", "public");

app.use("/assets", express.static(path.join(__dirname, "frontend")));

const server = new http.Server(app);
const io = ioServer(server);
const port = process.env.PORT || 8080;

let repository: CardsRepository = null;

mongoose.connect("mongodb://localhost:27017/cah", { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true } as mongoose.ConnectionOptions)
    .then(() => { repository = new CardsRepository(); })
    .catch((err) => { console.log("Mongodb connection error: " + err); });

app.use(bodyParser.json());

app.get("/", (req, res) => res.render("index"));

app.get("/api/game", (req: Request, res: Response) => {
    if (activeGame)
        res.status(200).send({ game: showJsonGame(activeGame) });
    else
        res.status(404).send({ msg: "Active game not found" });
});

const resetGame = async () => {
    const whiteDeck: WhiteDeck = shuffle(await repository.findAllWhitecards());
    const blackDeck: BlackDeck = shuffle(await repository.findOnePickBlackcards());

    return gameSnapshot.updateState(new GameCreated(createEmptyGame(blackDeck, whiteDeck)), activeGame);
};

app.post("/api/game/new", async (req: Request, res: Response) => {
    if (activeGame != null)
        res.status(500).send({ msg: "There is a game running already" });
    else {
        activeGame = await resetGame();
        res.status(200).send({ msg: "Game created", game: showJsonGame(activeGame) });
    }
});

app.post("/api/game/reset", async (req: Request, res: Response) => {
    activeGame = await resetGame();
    res.status(200).send({ msg: "Game created", game: showJsonGame(activeGame) });
});

app.post("/api/game/start", (req: Request, res: Response) => {
    if (activeGame === null) {
        res.status(500).send({ msg: "There is no running game" });
    } else if (activeGame.players.length < 3) {
        res.status(500).send({ msg: "There are not enough players. Minimun is 3" });
    } else {
        activeGame = gameSnapshot.updateState(new GameStarted(), activeGame);

        activeGame = gameSnapshot.updateState(new NextTurn(), activeGame);
        activeGame = gameSnapshot.updateState(new CzarChosen(chooseCzarForRound(activeGame)), activeGame);

        for (let index = 0; index < 10; index++) {
            activeGame.players.forEach((player) => {
                activeGame = gameSnapshot.updateState(new WhiteCardDealed(player), activeGame);
            });
        }

        activeGame = gameSnapshot.updateState(new BlackCardDrawed(), activeGame);

        res.status(200).send({ msg: "Game started", game: showJsonGame(activeGame) });
        io.emit("new_game_status", showJsonGame(activeGame));
    }
});

io.on("connection", (socket) => {
    socket.on("join_game", (req: string) => {
        if (activeGame === null) {
            io.emit("game_error", { msg: "Error joining: There is no active game" });
            return;
        }

        const { name } = JSON.parse(req);

        if (activeGame.state instanceof WaitingForPlayers) {
            activeGame = gameSnapshot.updateState(new PlayerJoined(name), activeGame);
            io.emit("new_game_status", showJsonGame(activeGame));
        } else {
            io.emit("game_error", { msg: "Error joining: Game already started" });
        }
    });

    socket.on("refresh_game", () => {
        socket.emit("new_game_status", showJsonGame(activeGame));
    });

    socket.on("white_card_submission", (req: string) => {
        const { player, whiteCard } = JSON.parse(req);

        if (activeGame.currentTurn.state instanceof WaitingForWhiteCards) {
            if (activeGame.currentTurn.cardCzar === player.name) {
                io.emit("game_error", { msg: "Error submitting card: Czar is not allowed to submit card" });
                return;
            }

            const hasPlayerSubmittedCardThisRound = (submissions: PlayerWhiteCardSubmission[], player: Player) =>
                submissions
                    .map(([_, splayer]) => splayer)
                    .filter((splayer) => player.name === splayer.name)
                    .length > 0;

            if (!hasPlayerSubmittedCardThisRound(activeGame.currentTurn.submissions, player)) {
                activeGame = gameSnapshot.updateState(new PlayerSubmittedWhiteCard([whiteCard, player]), activeGame);
                io.emit("new_game_status", showJsonGame(activeGame));
            } else {
                io.emit("game_error", { msg: "Error submitting card: Only one submission per turn is allowed" });
            }
        } else {
            io.emit("game_error", { msg: "Error submitting card: Czar is choosing round winner" });
        }
    });

    socket.on("czar_winner_choice", (req: string) => {
        const { playerName, whiteCard, czarName } = JSON.parse(req);

        if (activeGame.currentTurn.state instanceof ChoosingBestResponse) {
            if (activeGame.currentTurn.cardCzar === czarName) {
                const cardsToDraw = (activeGame.currentTurn.blackCard as PickOne) ? 1 : 2;
                const currentCzar = activeGame.currentTurn.cardCzar;
                const winner = activeGame.players.find((player) => player.name === playerName);
                activeGame = gameSnapshot.updateState(new CzarChoseRoundWinner(winner), activeGame);

                activeGame = gameSnapshot.updateState(new NextTurn(), activeGame);
                activeGame = gameSnapshot.updateState(new CzarChosen(chooseCzarForRound(activeGame)), activeGame);

                activeGame.players.filter((player) => player.name !== currentCzar).forEach((player) => {
                    for (let i = 0; i < cardsToDraw; i++) {
                        activeGame = gameSnapshot.updateState(new WhiteCardDealed(player), activeGame);
                    }
                });

                io.emit("new_game_status", showJsonGame(activeGame));
            } else {
                io.emit("game_error", { msg: "Error selecting winner: Only the round Czar can select a winner" });
            }
        } else {
            io.emit("game_error", { msg: "Error selecting winner: There are still players submitting responses" });
        }
    });
});

server.listen(port, () => console.log("Server listening on " + port));
