import React, { useEffect, useState } from "react"
import openSocket from 'socket.io-client'
import ReactDOM from "react-dom"
import { createMuiTheme, responsiveFontSizes, ThemeProvider } from "@material-ui/core/styles"
import Axios from "axios"
import { Player } from "./Player"
import { Admin } from "./Admin"
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"

const theme = responsiveFontSizes(createMuiTheme())

const socket = openSocket("https://d4a35346.ngrok.io/")

const App = () => {
    const [game, setGame] = useState(undefined)

    useEffect(() => {
        const setNewGameStatus = (game) => { setGame(game) }

        console.log("Declaring sockets listeners...")
        socket.on("new_game_status", (game) => {
            console.log("New game status arrived...")
            console.log(game)
            setNewGameStatus(game)
        })
        socket.on("game_error", (error) => console.log(error))

        return () => {
            console.log("Disconnecting socket")
            socket.close()
        }
    }, [])

    useEffect(() => {
        let ignore = false
        async function fetchGameStatus() {
            const response = await Axios.get("/api/game")
            setGame(response.data.game)
        }

        if (!ignore) fetchGameStatus()
        return () => { ignore = true }
    }, [])

    const createNewGame = async () => {
        const setNewGameStatus = (game) => { setGame(game) }

        const response = await Axios.post("/api/game/new")
        setNewGameStatus(response.data.game)
    }

    const startGame = async () => {
        const setNewGameStatus = (game) => { setGame(game) }

        const response = await Axios.post("/api/game/start")
        setNewGameStatus(response.data.game)
    }

    const connectToGame = (name) => {
        console.log(`(${name}) Connecting to game...`)
        socket.emit("join_game", JSON.stringify({ name: name }))
    }

    const sendWhiteCardSubmission = (myPlayer, whiteCard) => {
        console.log(`(${myPlayer.name}) Sending submission...`)
        const player = game.players.find((player) => myPlayer.name === player.name)
        socket.emit("white_card_submission", JSON.stringify({ player, whiteCard }))
    }

    return (
        <ThemeProvider theme={theme}>
            <Router>
                <Switch>
                    <Route path="/admin">
                        <Admin game={game} onStartGame={startGame} onCreateGame={createNewGame} />
                    </Route>
                    <Route path="/">
                        <Player game={game} onConnectGame={connectToGame} onCardSelected={sendWhiteCardSubmission} />
                    </Route>
                </Switch>
            </Router>
        </ThemeProvider>
    )
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)