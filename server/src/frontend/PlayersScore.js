import React from "react";
import { Paper, List, ListItem, ListItemText, Divider } from "@material-ui/core";

import "./styles.css";

export const PlayersScore = ({ players, czar }) => (
    <Paper style={{ float: "right", maxWidth: 150, width: "100%", position: "absolute", top: "0px", right: "37px" }}>
        <List>
            {players.map(({ name, awesomePoints }, index) =>
                <div key={name}>
                    <ListItem style={{ backgroundColor: (czar === name) ? "black" : "white" }}>
                        <ListItemText primary={name} secondary={`${awesomePoints} puntos`} id={ (czar === name) ? "czarScore" : "notCzarScore" } />
                    </ListItem>
                    {(index !== players.length - 1) && <Divider />}
                </div>
            )}
        </List>
    </Paper>
);