import React from "react";
import { Paper, Typography } from "@material-ui/core";

export const BlackCard = ({ blackCard, style }) => (
    <Paper style={{ ...style, backgroundColor: "black", padding: 22, minWidth: 250 }}>
        <Typography variant="h5" component="h3" style={{ color: "white", maxWidth: 350, marginBottom: 72 }}>
            {blackCard.statement.replace(/_/g, "___________")}
        </Typography>

        <Typography component="p" style={{ fontSize: 11, color: "white" }}>
            Cards Against Humanity
        </Typography>
    </Paper>
);
