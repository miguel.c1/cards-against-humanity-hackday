import React, { useState } from "react"
import { Typography, TextField, Button, Box } from "@material-ui/core"

export const ConnectToGame = ({ onConnectGame }) => {
    let [name, setName] = useState("")

    const handleNameChange = (input) => setName(input.target.value)

    return (
        <Box display="flex" flexDirection="column" justifyContent="space-between" style={{height: 150}}>
            <Typography variant="h5" component="h3">Escribe tu nombre</Typography>
            <TextField variant="outlined" label="Nombre" onChange={handleNameChange} />
            <Button variant="contained" color="primary" disabled={name === ""} onClick={() => onConnectGame(name)}>Entrar a la partida</Button>
        </Box>
    )
}