import React from "react"
import { Container, Typography } from "@material-ui/core"
import { OngoingGamePage } from "./OngoingGamePage"
import { CreateGamePage } from "./CreateGamePage"

export const Admin = ({ game, onStartGame, onCreateGame }) => (
    <div>
        <Container maxWidth="sm" style={{ marginTop: 64 }}>
            <Typography variant="h4" style={{ textAlign: "center" }}>Cards Against Humanity</Typography>
        </Container>

        {(game)
            ? <OngoingGamePage game={game} onStartGame={onStartGame} />
            : <CreateGamePage onCreateGame={onCreateGame} />
        }
    </div>
)