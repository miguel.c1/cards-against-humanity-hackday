import React from "react"
import { Grid, Paper, Typography } from "@material-ui/core"

export const NormalTurn = ({ game, myPlayer, onCardSelected }) => (
    <Grid container spacing={3}>
        {game.players.find((player) => myPlayer.name === player.name).hand.map((whiteCard, index) => (
            <Grid item xs key={index}>
                <WhiteCard card={whiteCard} onCardSelected={onCardSelected} />
            </Grid>
        ))}
    </Grid>
)

const WhiteCard = ({ card, onCardSelected }) => (
    <Paper style={{ padding: 16 }} onClick={() => onCardSelected(card)}>
        <Typography variant="h5" component="h3" style={{ marginBottom: 48 }}>{card.value}</Typography>

        <Typography component="p" style={{ fontSize: "0.5em" }}>
            Cards Against Humanity
        </Typography>
    </Paper>
);