import React from "react";
import { Typography, Box } from "@material-ui/core";
import { BlackCard } from "./BlackCard";
import {PlayerSubmissions} from "./PlayerSubmissions";

export const CurrentTurn = ({ game }) => {
    return (
        <Box display="flex" flexDirection="column" justifyContent="space-between" alignItems="center">
            <Typography variant="h5" component="h4">
                Ronda {game.currentTurn.round}
            </Typography>

            <BlackCard blackCard={game.currentTurn.blackCard} style={{marginTop: 42, marginBottom: 26}}/>

            { (game.currentTurn.submissions.length > 0) && <PlayerSubmissions turn={game.currentTurn} /> }
        </Box>
    );
};