import React from "react";
import { Container, Box } from "@material-ui/core";
import { BlackCard } from "./BlackCard";
import { StartGame } from "./StartGame";
import { PlayersScore } from "./PlayersScore";
import { CurrentTurn } from "./CurrentTurn";

export const OngoingGamePage = ({ game, onStartGame }) => {

    const handleGameStateRendering = (game) => {
        switch (game.state) {
            case "waiting_for_players": return <StartGame players={game.players} onStartGame={onStartGame} />;
            case "playing_turn": return <CurrentTurn game={game} />;
            default: return <BlackCard blackCard={game.currentTurn.blackCard} />;
        }
    };

    return (
        <Container maxWidth="xl" style={{ marginTop: 36 }}>
            <Box display="flex" justifyContent="center">
                {handleGameStateRendering(game)}
            </Box>

            { (game.players.length > 0) && <PlayersScore players={game.players} czar={game.currentTurn.cardCzar} /> }
        </Container>
    );
};