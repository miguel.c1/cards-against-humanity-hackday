import React from "react";
import {Grid, Paper, Typography} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

export const PlayerSubmissions = ({ turn }) => (
    <Grid container spacing={4}>
        { turn.submissions.map((submission, index) => (
            <Grid item key={index}>
                { (turn.blackCard.type === "pick_two")
                    ? (<>
                        <WhiteCard card={submission[0][0]} turnState={turn.state}/>
                        <WhiteCard card={submission[0][1]} turnState={turn.state}/>
                       </>)
                    : <WhiteCard card={submission[0]} turnState={turn.state}/>
                }
            </Grid>
        )) }
    </Grid>
);

const WhiteCard = ({ card, turnState }) => (
    <Paper style={{ padding: 22, minWidth: 150 }}>
        {(turnState === "choosing_best_response")
            ? <Typography variant="h5" component="h3" style={{ maxWidth: 250, marginBottom: 72 }}>{card.value}</Typography>
            : <Skeleton variant="rect" width={150} height={32} style={{ maxWidth: 250, marginBottom: 72 }}/>
        }

        <Typography component="p" style={{ fontSize: 11 }}>
            Cards Against Humanity
        </Typography>
    </Paper>
);
