import React from "react";
import Button from "@material-ui/core/Button";
import { Container, Box } from "@material-ui/core";

export const CreateGamePage = ({ onCreateGame }) => (
    <Container maxWidth="xl" style={{ marginTop: 36 }}>
        <Box display="flex" justifyContent="center">
            <Button variant="contained" color="primary" onClick={onCreateGame}>
                Crear partida
            </Button>
        </Box>
    </Container>
);
