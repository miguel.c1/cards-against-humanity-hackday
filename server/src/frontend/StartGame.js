import React from "react";
import { LinearProgress, Typography, Button, Box } from "@material-ui/core";

export const StartGame = ({ players, onStartGame }) => (
    <Box display="flex" justifyContent="space-between" flexDirection="column" height={250}>
        <Typography variant="h3" component="h3">
            Esperando a más jugadores...
        </Typography>

        <div>
            <LinearProgress variant="determinate" value={Math.min((players.length / 3) * 100, 100)}/>
            { (players.length < 3) 
                ? <Typography component="p" style={{textAlign: "center", marginTop: 6}}>Mínimo {players.length} / 3 jugadores para empezar</Typography> 
                : <Typography component="p" style={{textAlign: "center", marginTop: 6}}>{players.length} jugadores en la partida</Typography> 
            }
        </div>

        <Button disabled={players.length < 3} variant="contained" onClick={onStartGame}>
            Empezar partida
        </Button>
    </Box>
);