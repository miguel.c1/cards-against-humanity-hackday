import { Game, PlayerName, Player, createPlayer, drawCard, PlayerWhiteCardSubmission, Turn, PlayerPickTwoSubmission, PickTwo, PlayerPickOneSubmission } from "./Game";
import { reduce } from "lodash";
import { right, Either } from "fp-ts/lib/Either";

export class WaitingForPlayers { kind = "waiting_for_players" }
export class PlayingTurn { kind = "playing_turn" }
export class GameFinished { kind = "game_finished" }
export type GameState = WaitingForPlayers | PlayingTurn | GameFinished

export class WaitingForWhiteCards { kind = "waiting_for_white_cards" }
export class ChoosingBestResponse { kind = "choosing_best_response" }
export type TurnState = WaitingForWhiteCards | ChoosingBestResponse

export interface DomainEvent<A> {
    sentAt: Date;
}

export class GameCreated implements DomainEvent<Game> {
    sentAt: Date = new Date()
    game: Game

    constructor(newGame: Game) {
        this.game = newGame;
    }
}

export class PlayerJoined implements DomainEvent<Game> {
    sentAt: Date = new Date()
    playerName: PlayerName

    constructor(pN: PlayerName) {
        this.playerName = pN;
    }
}

export class GameStarted implements DomainEvent<Game> {
    sentAt: Date = new Date()
}

export class WhiteCardDealed implements DomainEvent<Game> {
    sentAt: Date = new Date()
    player: Player

    constructor(p: Player) {
        this.player = p;
    }
}

export class CzarChosen implements DomainEvent<Game> {
    sentAt: Date = new Date()
    player: Player

    constructor(czar: Player) {
        this.player = czar;
    }
}

export class BlackCardDrawed implements DomainEvent<Game> {
    sentAt: Date = new Date()
}

export class PlayerSubmittedWhiteCard implements DomainEvent<Game> {
    sentAt: Date = new Date()
    submission: PlayerWhiteCardSubmission

    constructor(sub: PlayerWhiteCardSubmission) {
        this.submission = sub;
    }
}

export class CzarChoseRoundWinner implements DomainEvent<Game> {
    sentAt: Date = new Date()
    roundWinner: Player

    constructor(rWinner: Player) {
        this.roundWinner = rWinner;
    }
}

export class NextTurn implements DomainEvent<Game> {
    sentAt: Date = new Date()
}

abstract class Snapshot<A> {
    public abstract updateState(ev: DomainEvent<any>, initial: A): A

    public snapshot(evs: DomainEvent<any>[]): Either<string, A> {
        return right(reduce(evs.reverse(), (result, value) => this.updateState(value, result), {} as A));
    }
}

export class GameSnapshot extends Snapshot<Game> {
    public updateState(ev: DomainEvent<any>, initial: Game): Game {
        if (ev instanceof GameCreated) {
            return ev.game;
        } else if (ev instanceof PlayerJoined) {
            return { ...initial, players: initial.players.concat(createPlayer(ev.playerName)) };
        } else if (ev instanceof GameStarted) {
            return { ...initial, state: new PlayingTurn() };
        } else if (ev instanceof WhiteCardDealed) {
            const [card, deck] = drawCard(initial.whiteDeck);
            const updatedPlayer = { ...ev.player, hand: ev.player.hand.concat(card) };
            return { ...initial, whiteDeck: deck, players: initial.players.map((player) => (player.name === updatedPlayer.name) ? updatedPlayer : player) };
        } else if (ev instanceof CzarChosen) {
            return { ...initial, currentTurn: { ...initial.currentTurn, cardCzar: ev.player.name } };
        } else if (ev instanceof BlackCardDrawed) {
            const [card, deck] = drawCard(initial.blackDeck);
            return { ...initial, blackDeck: deck, currentTurn: { ...initial.currentTurn, blackCard: card, state: new WaitingForWhiteCards() } };
        } else if (ev instanceof PlayerSubmittedWhiteCard) {
            const [_, player] = ev.submission;
            let updatedPlayer: Player;
            if (initial.currentTurn.blackCard.kind === "pick_two") {
                const [[fstCard, sndCard], _] = ev.submission as PlayerPickTwoSubmission;
                updatedPlayer = { ...player, hand: player.hand.filter((card) => card.value !== fstCard.value && card.value !== sndCard.value) };
            } else {
                const [scard, _] = ev.submission as PlayerPickOneSubmission;
                updatedPlayer = { ...player, hand: player.hand.filter((card) => card.value !== scard.value) };
            }
            const newTurnState = (initial.currentTurn.submissions.length + 1 === initial.players.length - 1) ? new ChoosingBestResponse() : new WaitingForWhiteCards();
            return {
                ...initial,
                currentTurn: { ...initial.currentTurn, submissions: initial.currentTurn.submissions.concat([ev.submission]), state: newTurnState },
                players: initial.players.map((player) => (player.name === updatedPlayer.name) ? updatedPlayer : player)
            };
        } else if (ev instanceof CzarChoseRoundWinner) {
            return {
                ...initial,
                players: initial.players.map((player) => (player.name === ev.roundWinner.name) ? { ...player, awesomePoints: player.awesomePoints + 1 } : player)
            };
        } else if (ev instanceof NextTurn) {
            const [nextBlackCard, remainingDeck] = drawCard(initial.blackDeck);
            return { 
                ...initial,
                blackDeck: remainingDeck,
                currentTurn: { round: initial.currentTurn.round + 1, blackCard: nextBlackCard, submissions: [], state: new WaitingForWhiteCards() } as Turn 
            };
        }
    }
}

