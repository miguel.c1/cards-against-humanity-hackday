import { maxBy, head, tail, find } from "lodash";
import { GameState, TurnState, WaitingForPlayers, WaitingForWhiteCards } from "./States";

export interface Game {
    players: Player[];
    blackDeck: BlackDeck;
    whiteDeck: WhiteDeck;
    currentTurn: Turn;
    state: GameState;
}

export function showJsonBlackCard(blackCard: BlackCard): JsonBlackCard {
    if (!blackCard) return { statement: "", type: "pick_one" };

    return { statement: blackCard.statement, type: blackCard.type };
}

export function showJsonTurn(turn: Turn): JsonTurn {
    return {
        round: turn.round,
        cardCzar: turn.cardCzar,
        blackCard: showJsonBlackCard(turn.blackCard),
        submissions: (turn.submissions) ? turn.submissions : [],
        state: turn.state.kind
    } as JsonTurn;
}

export function showJsonGame(game: Game): JsonGame {
    return {
        players: game.players,
        currentTurn: showJsonTurn(game.currentTurn),
        state: game.state.kind,
        blackDeck: game.blackDeck.length,
        whiteDeck: game.whiteDeck.length
    } as JsonGame;
}

export interface JsonGame {
    players: Player[];
    currentTurn: JsonTurn;
    state: string;
    blackDeck: number;
    whiteDeck: number;
}

export interface JsonTurn {
    round: number;
    cardCzar: PlayerName;
    blackCard: JsonBlackCard;
    submissions: PlayerWhiteCardSubmission[];
    state: string;
}

export interface JsonBlackCard {
    statement: string;
    type: string;
}

export interface Player {
    name: PlayerName;
    awesomePoints: number;
    hand: Hand;
}

export type PlayerName = string

export type BlackCard = PickOne | PickTwo

export interface PickOne {
    kind: "pick_one";
    statement: string;
    type: string;
}

export interface PickTwo {
    kind: "pick_two";
    statement: string;
    type: string;
}

export interface WhiteCard {
    value: string;
}

export interface Turn {
    round: number;
    cardCzar: PlayerName;
    blackCard: BlackCard;
    submissions: PlayerWhiteCardSubmission[];
    state: TurnState;
}

export type BlackDeck = BlackCard[]
export type WhiteDeck = WhiteCard[]
export type Hand = WhiteCard[]

export type PlayerPickOneSubmission = [WhiteCard, Player]
export type PlayerPickTwoSubmission = [[WhiteCard, WhiteCard], Player]
export type PlayerWhiteCardSubmission = PlayerPickOneSubmission | PlayerPickTwoSubmission

export type Winner = Player

export function createEmptyGame(blackDeck: BlackDeck, whiteDeck: WhiteDeck): Game {
    return {
        players: [],
        blackDeck,
        whiteDeck,
        currentTurn: { round: 0, cardCzar: "", state: new WaitingForWhiteCards() } as Turn,
        state: new WaitingForPlayers()
    } as Game;
}

export function createPlayer(playerName: PlayerName): Player {
    return { name: playerName, awesomePoints: 0, hand: [] } as Player;
}

export function getCurrentCzar(game: Game): Player {
    return find(game.players, (player) => game.currentTurn.cardCzar === player.name);
}

export function finishGame(game: Game): Winner {
    return maxBy(game.players, (player) => player.awesomePoints);
}

export function drawCard<A>(deck: A[]): [A, A[]] {
    const drawedCard = head(deck);
    const remainingDeck = tail(deck);
    return [drawedCard, remainingDeck];
}

export function chooseCzarForRound(game: Game): Player {
    return game.players[(game.currentTurn.round - 1) % game.players.length];
}

